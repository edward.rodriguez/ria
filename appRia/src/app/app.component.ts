import { Component } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { FormRegistroComponent } from './registro/form-registro/form-registro.component';
import { FormLoginComponent } from './login/form-login/form-login.component';
import {FormControl} from '@angular/forms';
import { AsignarRolComponent } from './modales/roles/asignar-rol/asignar-rol.component';
import { QuitarRolComponent } from './modales/roles/quitar-rol/quitar-rol.component';
import { CanActivate, Router } from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'appRia';
  mode = new FormControl('over');
  shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));
  
  constructor(private router: Router,public dialog: MatDialog) {}

  openDialogRegistro(){
      this.dialog.open(FormRegistroComponent);
  }
  openDialogLogin(){
    this.dialog.open(FormLoginComponent);
}

openDialogAsignar(){
  this.dialog.open(AsignarRolComponent);
}
openDialogQuitar(){
  this.dialog.open(QuitarRolComponent);
}

Logout(){
  localStorage.removeItem("roles")
  localStorage.removeItem("token")
  this.router.navigate(['/']);
}

isAdmin(){
  if(localStorage.getItem('roles') == null ){
    return false
  }
  if(localStorage.getItem('roles').includes( 'ADMIN' ) ){
    return true
  }
  return false;
}

isUser(){
  if(localStorage.getItem('roles') == null ){
    return false
  }
  if(localStorage.getItem('roles').includes( 'USER' ) ){
    return true
  }
  return false;
}

isSecre(){
  if(localStorage.getItem('roles') == null ){
    return false
  }
  if(localStorage.getItem('roles').includes( 'SECRETARIA' ) ){
    return true
  }
  return false;
}

isSesion(){

  if(localStorage.getItem('token') == null ){
    return false
  }
  return true;    

}


}
